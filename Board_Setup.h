#ifndef BOARD_SETUP_H
#define BOARD_SETUP_H

#include "Warship.h"

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

class Board_Setup
{
	public:
		Board_Setup();

		vector<Warship> place_ships(string player, vector<Warship> bs_v);

	private:
		char integer_conversion(int i);

		bool ship_compare(Warship bs, vector<Warship> other_bs);

		Warship placement(Warship bs, vector<Warship> other_bs);
};

#endif
