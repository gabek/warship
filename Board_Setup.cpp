
#include "Board_Setup.h"

using namespace std;

Board_Setup::Board_Setup()
{
}

char Board_Setup::integer_conversion(int i)
{
	map<int,char> conv;

	conv[0]='A';
	conv[1]='B';
	conv[2]='C';
	conv[3]='D';
	conv[4]='E';
	conv[5]='F';
	conv[6]='G';
	conv[7]='H';
	conv[8]='I';
	conv[9]='J';
	return conv.at(i);
}

bool Board_Setup::ship_compare(Warship bs, vector<Warship> other_bs)
{
	for(int i = 0; i < other_bs.size(); i++)
		{
				
			Warship temp_bs =  other_bs.at(i);
			
			int bs_min_row = min(bs.get_row_start(),bs.get_row_end());

			int bs_max_row = max(bs.get_row_end(),bs.get_row_start());

			int bs_min_col = min(bs.get_column_start(),bs.get_column_start());

			int bs_max_col = max(bs.get_column_start(),bs.get_column_end());

			int temp_min_row = min(temp_bs.get_row_start(),temp_bs.get_row_end());

			int temp_max_row = max(temp_bs.get_row_end(),temp_bs.get_row_start());

			int temp_min_col = min(temp_bs.get_column_start(),temp_bs.get_column_end());

			int temp_max_col = max(temp_bs.get_column_end(),temp_bs.get_column_start());				
	
			for(int j = bs_min_row; j <= bs_max_row; j++)
			{
				for(int k = bs_min_col; k <= bs_max_col; k++)
					{
						if(k >= temp_min_col && k <= temp_max_col && j >= temp_min_row && j <= temp_max_row) //not as unholy as it used to be
						{
							cout << bs.get_name() << " overlaps with " << temp_bs.get_name() << " at row " <<
							       	integer_conversion(j) << " and column " << (k+1) << ". Please change location\n";
							return false;
						}
					}
			}
		}
	return true;
}

Warship Board_Setup::placement(Warship bs, vector<Warship> other_bs)
{
	bool loop1 = false;

	char row;

	int column; 

	int dir;

	while(loop1 == false)
	{
		cout << "Which row (A-J) would you like to place " << bs.get_name() << ".\n";

		bool loop2 = false;
		
		while(loop2 == false)
		{
			if((cin >> row))
			{
				if(row >= 'A' && row <= 'J')
				{
					cout << "Row is " << row << ".\n";

					loop2 = true;
				}
				else
					cout << "Error: invalid row. Must be a capital letter and between A and J.\n";
			}
			else
			{
				cerr << "Bad input. Pleae enter an char.\n";
				cin.clear();
      				cin.ignore(10000,'\n');
			}	
		}

		loop2 = false;
		
		cout << "Which column (1-10) would you like to place " << bs.get_name() << ".\n";

		while(loop2 == false)
		{
			if((cin >> column))
			{
				if(column >= 1 && column <= 10)
				{
					cout << "Column is " << column << ".\n";

					loop2 = true;
				}
				else
					cout << "Error: invalid column. Must be between 1 and 10.\n";
			}
			else
			{
				cerr << "Bad input. Pleae enter an int.\n";
				cin.clear();
      				cin.ignore(10000,'\n');
			}
		}
		
		loop2 = false;

		cout << "Please enter a number for the direction you want the ship to face.\nNorth is 1\nEast is 2\nSouth is 3\nWest is 4\n";	
		while(loop2 == false)
		{
			if(cin >> dir)
			{
				if(dir >= 1 && dir <= 4)
				{
					switch(dir)
					{
						case 1: cout << "The direction is north\n";
							break;

						case 2: cout << "The direction is east\n";
							break;

						case 3: cout << "The direction is south\n";
							break;

						case 4: cout << "The direction is west\n";
							break;
					}

					loop2 = true;

					bs.set_direction(dir);
				}
			}
			else
			{
				cerr << "Bad input. Pleae enter an int.\n";
				cin.clear();
      				cin.ignore(10000,'\n');
			}
		}
	
		if(bs.fit(row, column))
		{
			loop1 = ship_compare(bs, other_bs);
		}
		if(loop1)
		{
			cout << bs.get_name() << " is from " << row << column << " to " << integer_conversion(bs.get_row_end()) << (bs.get_column_end() + 1) << "\n";
			cout << "Is this correct. Enter no if you want to change it (other wise enter anything else)\n";
			
			string response;
			if(!(cin >> response))
			{
				cerr << "Congrats. I don't know how but you broke a string\n";
				cin.clear();
      				cin.ignore(10000,'\n');
			}

			for(int i = 0; i < response.size(); i++)
			{
				char c = response.at(i);

				c = tolower(c);

				response[i] = c;
			}
			if(response.compare("no") == false)
				loop1 = false;
		}
	}

	
	return bs;
}


vector<Warship> Board_Setup::place_ships(string player, vector<Warship> bs_v)
{
	Warship aircraft((player + "'s Aircraft Carrier"),5,1);

	aircraft = placement(aircraft, bs_v);

	bs_v.push_back(aircraft);

	Warship battleship((player + "'s Battleship"),4,1);

	battleship = placement(battleship, bs_v);

	bs_v.push_back(battleship);
		
	Warship cruiser((player + "'s Cruiser"),3,1);
		
	cruiser = placement(cruiser, bs_v);

	bs_v.push_back(cruiser);

	Warship submarine((player + "'s Submarine"),3,1);
		
	submarine = placement(submarine, bs_v);

	bs_v.push_back(submarine);

	Warship destroyer((player + "'s Destroyer"),2,1);

	destroyer = placement(destroyer, bs_v);

	bs_v.push_back(destroyer);

	return bs_v;
}
