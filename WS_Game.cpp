

/* Author: Gab Kelly
 * This is a program mimicking a game of Warship.
 * It requires 2 players and currently is command line only.
 *
 * TODO: Change to War ship
 * TODO: IMPLEMENT RULE OF FIVE
 *
 */
#include "Warship.h"
#include "Board_Setup.h"
#include "WS_Manager.h"

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

string name(int num)
{
	string player;
	bool exit = false;
	while(exit == false)
	{
		cout << "Player " << num << " enter your name: ";

		if(!(cin >> player))
		{
			cerr << "Congrats. I don't know how but you broke a string\n";
			cin.clear();
      			cin.ignore(10000,'\n');
			return "Congrats";
		}

		cout << "Player " << num << " is : " << player << ". \n Is this correct? Enter no if you want to change it (otherwise enter anything else).\n";
		
		string response;

		if(!(cin >> response))
		{
			cerr << "Congrats. I don't know how but you broke a string\n";
			cin.clear();
      			cin.ignore(10000,'\n');
			return player;
		}

		for(int i = 0; i < response.size(); i++)
		{
			char c = response.at(i);

			c = tolower(c);

			response[i] = c;
		}
		if(response.compare("no"))
			exit = true;
	}

	return player;

}

void display_grid(char grid[][10])
{
	cout << "   1  2  3  4  5  6  7  8  9  10\n";
	int row = 0;
	for(char i = 'A'; i <= 'J'; i++)
	{
		cout << i;
		for(int j = 0; j < 10; j++)
		{
			cout << "  " << grid[row][j];
		}
		cout << "\n";

		row++;
	}
}

int row_conversion(char c)
	{
		map<char,int> conv;
			conv['A']=1;
			conv['B']=2;
			conv['C']=3;
			conv['D']=4;
			conv['E']=5;
			conv['F']=6;
			conv['G']=7;
			conv['H']=8;
			conv['I']=9;
			conv['J']=10;

		return conv.at(c);
	}

vector<int> turn(string player, char grid[][10])
{
	display_grid(grid);

	bool loop1 = true;
	bool loop2 = true;

	vector<int> out;

	char row;
	int col;

	cout << player << " which row (A-J) do you want to attack?\n";
	do
	{
		do
		{
			if((cin >> row))
			{
				if(row >= 'A' && row <= 'J')
					loop2 = false;
				else
					cout << "Error: invalid row. Must be a capital letter and between A and J.\n";
			}
			else
			{
				cerr << "Bad input. Pleae enter an char.\n";
				cin.clear();
				cin.ignore(10000,'\n');
			}
		} while(loop2 == true);
	
		loop2 = true;
		out.push_back(row_conversion(row)-1);

		cout << player << " which column (1-10) do you want to attack?\n";
		do
		{
			if((cin >> col))
			{
				if(col >= 1 && col <= 10)
					loop2 = false;
				else
					cout << "Error: invalid column. Must be a number 1 through 10\n";
			}
			else
			{
				cerr << "Bad input. Pleae enter an int.\n";
				cin.clear();
				cin.ignore(10000,'\n');
			}
		} while(loop2 == true);

		out.push_back(col-1);
		if(grid[out.at(0)][out.at(1)] == '*')
			loop1 = false;
		else
		{
			cout << "The target was already attacked once. Select a new location\n";
			out.clear();
		}
	}while(loop1 == true);

	return out;
}

void clear_screen()
{
    cout << string( 100, '\n' );
}

int main()
{

	bool grid1a[10][10];

	bool grid2a[10][10];
	
	char grid1d[10][10];

	char grid2d[10][10];

	Board_Setup setup;

	WS_Manager manager;

	vector<Warship> Warship_P1;

	vector<Warship> Warship_P2;
	
	enum dir{North=1, East = 2, South = 3, West = 4};

	bool exit = false;

	string player1;

	string player2;

	for(int i = 0; i < 10; i++)
	{
		for(int j = 0; j < 10; j++)
		{
			grid1a[i][j] = false;

			grid2a[i][j] = false;

			grid1d[i][j] = '*';

			grid2d[i][j] = '*';

		}
	}
	
	player1 = name(1);
	player2 = name(2);

	
	cout << player1 << " will place their ships first. " << player2 << " please look away.\n";

	Sleep(500);

	Warship_P1 = setup.place_ships(player1, Warship_P1);

	clear_screen();

	cout << player2 << " will now place their ships. " << player1 << " please look away.\n";

	Warship_P2 = setup.place_ships(player2, Warship_P2);
	
	clear_screen();

	exit = false;
	
	while(exit == false)
	{

		vector<int> coord = turn(player1, grid1d);
	
		grid1d[coord.at(0)][coord.at(1)] = manager.attack_point(coord.at(0), coord.at(1), &Warship_P2);

		for(int i = 0; i < Warship_P2.size(); i++)
			if(Warship_P2.at(i).sunk())
			{
				Warship_P2.erase(Warship_P2.begin() + i);
				break;
			}
		
		coord.clear();

		if(Warship_P2.empty())
		{
			cout << player1 << " is VICTORIOUS!"; 
			break;
		}

		coord = turn(player2, grid2d);

		grid2d[coord.at(0)][coord.at(1)] = manager.attack_point(coord.at(0), coord.at(1), &Warship_P1);

		for(int i = 0; i < Warship_P1.size(); i++)
			if(Warship_P1.at(i).sunk())
			{
				Warship_P1.erase(Warship_P1.begin() + i);
				break;
			}
		
		
		coord.clear();

		if(Warship_P1.empty())
		{
			cout << player2 << " is VICTORIOUS!"; 
			break;
		}		
	}
}
