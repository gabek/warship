/*
* Author: Gabriel Kelly
* Warship.
* This is a program that creates a class for a Warship
* game.
*/

#include "Warship.h"
#include <iostream>
#include <vector>
#include <map>


using namespace std;

 
		Warship::Warship(string n, int l, int d=1)
		{
			name = n;

			length = l;
			
			health = l;

			direction = d;
		}

		Warship::Warship(const Warship& other)
		{
			name = other.name;
			length = other.length;
			health = other.health;
			direction = other.direction;
			row_start = other.row_start;
			row_end = other.row_end;
			column_start = other.column_start;
			column_end = other.column_end;
		}

		Warship::~Warship() noexcept
		{
			delete[] &name;
			delete &length;
			delete &health;
			delete &direction;
			delete &row_start;
			delete &row_end;
			delete &column_start;
			delete &column_end;
		}

		Warship::Warship(Warship&& other) noexcept
		{
			name = other.name;
			length = other.length;
			health = other.health;
			direction = other.direction;
			row_start = other.row_start;
			row_end = other.row_end;
			column_start = other.column_start;
			column_end = other.column_end;
			
			other.name = nullptr;
			other.length = 0;
			other.health = 0;
			other.direction = 1;
			other.row_start = 0;
			other.row_end = 0;
			other.column_start = 0;
			other.column_end = 0;	
		}

		Warship& Warship::operator= (const Warship& other)
		{
			Warship copy(other);
			*this = move(copy);
			return *this;
		}
		
		Warship& Warship::operator= (Warship&& other) noexcept
		{
			delete[] &name;
			delete &length;
			delete &health;
			delete &direction;
			delete &row_start;
			delete &row_end;
			delete &column_start;
			delete &column_end;
			
			name = other.name;
			length = other.length;
			health = other.health;
			direction = other.direction;
			row_start = other.row_start;
			row_end = other.row_end;
			column_start = other.column_start;
			column_end = other.column_end;

			other.name = nullptr;
			other.length = 0;
			other.health = 0;
			other.direction = 0;
			other.row_start = 0;
			other.row_end = 0;
			other.column_start = 0;
			other.column_end = 0;

			return *this;
		}


		bool Warship::sunk()
		{
			if(health <= 0)
				return true;
			else
				return false;
		}

		int Warship::letter_conversion(char c)
		{
			map<char,int> conv;

			conv['A']=1;
			conv['B']=2;
			conv['C']=3;
			conv['D']=4;
			conv['E']=5;
			conv['F']=6;
			conv['G']=7;
			conv['H']=8;
			conv['I']=9;
			conv['J']=10;

			return conv.at(c);
		}

		
		bool Warship::fit(char r, int column)
		{
			int row = letter_conversion(r);

			bool complete = false;

			int len = length - 1;

			switch(direction)
				{
					case 1: if(row - len > 0 && row <= 10)
						{
							column_start = column_end = column-1;
							row_start = row - 1;
							row_end = row_start - len;
							complete = true;
						}
						else
							cout << "Ship cannot be placed there\n";
						break;
					
					case 2: if(column + len <= 10 && column > 0)
						{
							row_end = row_start = row-1;
							column_start = column - 1;
							column_end = column_start + len;
							complete = true;
						}
						else
							cout << "Ship cannot be placed there\n";
						break;
					

					case 3: if(row + len <= 10 && row > 0)
						{
							column_start = column_end = column-1;
							row_start = row - 1;
							row_end = row_start + len;
							complete = true;
						}
						else
							cout << "Ship cannot be placed there\n";
						break;

					case 4: if(column - len > 0 && column <= 10)
						{
							row_start = row_end = row-1;
							column_start = column - 1;
							column_end = column_start - len; 
							complete = true;
						}
						else
							cout << "Ship cannot be placed there\n";
						break;

				}
			return complete;
		}

		bool Warship::is_hit()
		{
			cout << "A SHIP WAS HIT!\n";

			health = health - 1;
		
			if(sunk())
				cout << "YOU SUNK " << name << "\n";

			return sunk();
		}

		int Warship::get_direction()
		{
			return direction;
		}

		void Warship::set_direction(int d)
		{
			direction = d;
		}

		string Warship::get_name()
		{
			return name;
		}
		
		void Warship::set_name(string n)
		{
			name = n;
		}

		int Warship::get_row_start()
		{
			return row_start;
		}
		
		int Warship::get_column_start()
		{
			return column_start;
		}
		
		int Warship::get_row_end()
		{
			return row_end;
		}
		
		int Warship::get_column_end()
		{
			return column_end;
		}
		
		int Warship::get_health()
		{
			return health;
		}
