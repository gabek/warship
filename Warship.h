/*
 * The header file for the Warship class
 */

#ifndef WARSHIP_H
#define WARSHIP_H
#include <string>

using namespace std;
class Warship
{
	public:


		Warship(string n, int l, int d);

		Warship(const Warship& other);

		~Warship() noexcept;

		Warship(Warship&& other) noexcept;

		Warship& operator= (const Warship& other);

		Warship& operator= (Warship&& other) noexcept;
		
		bool sunk();

		int letter_conversion(char c);

		bool fit(char r, int column);

		bool is_hit();

		int get_direction();

		void set_direction(int d);

		string get_name();

		void set_name(string n);

		int get_row_start();

		int get_column_start();

		int get_row_end();

		int get_column_end();

		int get_health();

	private:
		string name;

		int row_start;

		int row_end;

		int column_start;

		int column_end;

		int length;

		int health;

		int direction;
};

#endif
