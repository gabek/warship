#ifndef WS_MANAGER_H
#define WS_MANAGER_H

#include "Warship.h"

#include <vector>
#include <algorithm>
#include <iostream>


using namespace std;

class WS_Manager
{
	public:
		WS_Manager();

		char attack_point(int row, int col, vector<Warship> *t);

	private: 

		bool ship_hit(int row, int col, Warship *s);
};
#endif
