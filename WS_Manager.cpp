/*
* Author: Gab Kelly
* Manager for the gameplay of Warship
*/

#include "WS_Manager.h"

using namespace std;

WS_Manager::WS_Manager()
{	
}



char WS_Manager::attack_point(int row, int col, vector<Warship> *t)
	{	
		vector<Warship> ships = *t;
		char out;
		bool hit = false;
		for(int i = 0; i < ships.size(); i++)
		{
			hit = ship_hit(row, col, &ships.at(i));
			if(hit)
				break;
		}
		if(hit)
			out = 'H';
		else
		{
			cout << "MISS! SPLOOSH!";
			out = 'M';
		}

		*t = ships;

		return out;
	}

bool WS_Manager::ship_hit(int row, int col, Warship *s)
{
	Warship ship = *s;

	int min_row = min(ship.get_row_start(), ship.get_row_end());

	int max_row = max(ship.get_row_end(), ship.get_row_start());

	int min_col = min(ship.get_column_start(), ship.get_column_end());

	int max_col = max(ship.get_column_end(), ship.get_column_start());

	if(row >= min_row && row <= max_row && col >= min_col && col <= max_col)
	{
		ship.is_hit();

		*s = ship;

		return true;
	}
	else	
		return false;
}
