# README #

Warships is a fair and legal implementation of Battleship (the board game) in C++ as a weekend project. Currently runs only in the command line.

### About ###

* Weekend project to learn C++
* Evolving Project

### How do I get set up? ###

* git clone, download, or other means acquire the files in the repository.
* Enter the repository where you downloaded the project.
* g++ -std=c++0x WS_Game.cpp Warship.cpp Warship.h Board_Setup.cpp Board_Setup.h WS_Manager.cpp WS_Manager.h

### About Contributing ###

* This is just public to be linked to in a resume. Feel free to play the game, I just want to practice and gain experience.

### Who do I talk to? ###

* gabekly@gmail.com